#include <cstring>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cctype>
#include <string>
#include <vector>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <ctime>
#include <limits>
#include <stdexcept>
#include <chrono>
#include <thread>
#include <unordered_map>


#include "data.h"
#include "PG.h"
#include "VI.h"
#include "WJ.h"

using namespace std;

unordered_map<string, int> tag_id;
vector<string> id_tag;

int N;
vector<Pic*> pics;

void readInput(istream& is)
{
	is >> N;
	for (int _i = 0; _i < N; ++_i)
	{
		string dir; is >> dir;
		int _dir = HORIZ;
		if (dir == "V") _dir = VERT;
		auto *_Pic = new Pic(_i, 0, _dir);
		int M; is >> M;
		pics.push_back(_Pic);
		for (int _j = 0; _j < M; ++_j)
		{
			string tag; is >> tag;

			auto it = tag_id.find(tag);
			int tid = -1;
			if (it == tag_id.end()) {
				tid = id_tag.size();
				id_tag.push_back(tag);
				tag_id[tag] = tid;
			}
			else tid = it->second;
			_Pic->tagIDs.push_back(tid);
		}
		sort(_Pic->tagIDs.begin(), _Pic->tagIDs.end());
	}
}


void writeInput() {
	cout << "N: " << " " << N << endl;
	cout << Pic::ToString(pics) << endl;
}

int calcSCore(const Pic& p1, const Pic& p2, int &common)
{
	int i = 0, j = 0;
	common = 0;

	while ((i < p1.tagIDs.size()) && (j < p2.tagIDs.size()))
	{
		if (p1.tagIDs[i] < p2.tagIDs[j])
		{
			i++;
		}
		else if (p1.tagIDs[i] == p2.tagIDs[j])
		{
			i++;
			j++;
			common++;
		}
		else
		{
			j++;
		}
	}
	int scores[3];
	scores[0] = p1.tagIDs.size() - common;
	scores[1] = p2.tagIDs.size() - common;
	scores[2] = common;
	return min(min(scores[0], scores[1]), scores[2]);
}


vector<Edge> edges;
vector<vector<Neigh>> sz;
vector<bool> usedPic;
vector<int> fokszam;

vector<pair<int, int>> eredmeny;
int fullScore = 0;

void showEredmenyDetailed() {
	cout << "Eredmeny: " << endl;
	for (size_t i = 0; i < eredmeny.size(); i++)
		cout << "(" << eredmeny[i].first << ", " << eredmeny[i].second << ")" << endl;
	cout << "fullScore: " << fullScore << endl;
	cout << endl;
}

void showEredmeny(ostream& os) {
	os << eredmeny.size() << endl;
	for (size_t i = 0; i < eredmeny.size(); i++)
		if (eredmeny[i].second >= 0)
			os << eredmeny[i].first << " " << eredmeny[i].second << endl;
		else
			os << eredmeny[i].first << endl;
}

int lephet(int& n1, int& n2)
{
	//cout << n1 << " " << n2 << endl;
	int pos = -1;
	float max = -1;
	int honnan = n1;
	n1 = -1;
	for (size_t i = 0; i < sz[honnan].size(); i++)
	{
		int k1 = sz[honnan][i].P1;
		if (!usedPic[k1]) {
			int sc1 = sz[honnan][i].Score;
			int k2 = -1;
			if (pics[k1]->Dir == VERT) {
				// elso szabad vert kell most
				for (size_t j = 0; j < N; j++)
					if (!usedPic[j] && pics[j]->Dir == VERT && j != k1)
					{
						k2 = j;
						break;
					}
				if (k2 == -1) k1 = -1;
			}
			if (max < sc1) {
				max = sc1;
				n1 = k1;
				n2 = k2;
			}
		}
	}
	return max;
}
void lep(int n1, int n2)
{
	usedPic[n1] = true;
	if (n2 > -1)
		usedPic[n2] = true;
	eredmeny.push_back(make_pair(n1, n2));
}
void MOHO_0() {
	fokszam.resize(N);
	for (size_t i = 0; i < N; i++)
		fokszam[i] = sz[i].size();
	usedPic.resize(N, false);

	int startPos = -1;
	for (size_t i = 0; i < N; i++)
	{
		if (pics[i]->Dir == HORIZ) {
			startPos = i;
			break;
		}
	}

	eredmeny.clear();
	fullScore = 0;
	int kep1 = startPos; int kep2 = -1;
	if (startPos == -1) {
		kep1 = 0;
		kep2 = 1;
	}
	lep(kep1, kep2);
	while (true) {
		int max;
		max = lephet(kep1, kep2);
		if (kep1 == -1) break;
		lep(kep1, kep2);
		fullScore += max;
	}
	showEredmenyDetailed();
	ofstream os("out.txt");
	showEredmeny(os);
	os.close();
}


int main()
{
	string dir = "D:\\HashCodeVerseny\\CppGems\\";
	dir = "";
	string f1 = dir + "a_example.txt";
	string f2 = dir + "b_lovely_landscapes.txt";
	string f3 = dir + "c_memorable_moments.txt";
	string f4 = dir + "d_pet_pictures.txt";
	string f5 = dir + "e_shiny_selfies.txt";
	ifstream ifs(f5);


	readInput(ifs);
	//writeInput();

	cout << "start" << endl;

	int neighSzam = 0;
	sz.resize(N);
	int limit = 300;
	for (size_t i = 0; i < N; i++)
	{
		for (size_t j = i + 1; j < N; j++)
		{
			if ((sz[i].size() < limit || sz[j].size() < limit)) {
				int common;
				int score = calcSCore(*pics[i], *pics[j], common);
				if (score > 0) {
					//cout << pics[i] << endl << pics[j] << endl << score << endl;
					//edges.push_back(Edge(i, j, score));
					neighSzam += 2;
					if (sz[i].size() < limit) sz[i].push_back(Neigh(j, score, common));
					if (sz[j].size() < limit) sz[j].push_back(Neigh(i, score, common));
				}
			}
		}
		if (i % 1000 == 0)
			cout << i << " " << neighSzam << endl;
	}
	Edge::SortByScore(edges);
	reverse(edges.begin(), edges.end());
	/*
	cout << Edge::ToString(edges) << endl;
	for (size_t i = 0; i < N; i++)
	{
		cout << "Pic szomszedok: " << i << endl;
		cout << Neigh::ToString(sz[i]) << endl;
	}
	*/
	cout << "start moho" << endl;

	MOHO_0();





	cout << "DONE" << endl;
	getchar();
	return 0;
}