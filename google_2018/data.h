#pragma once

#include <cstring>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cctype>
#include <string>
#include <vector>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <ctime>
#include <limits>
#include <stdexcept>
#include <chrono>
#include <thread>
#include <unordered_map>

using namespace std;

#define HORIZ 0
#define VERT 1

extern unordered_map<string, int> tag_id;
extern vector<string> id_tag;

struct Pic
{
	int ID;
	int Value;
	int Dir;
	vector<int> tagIDs;

	Pic() : ID(0), Value(0), Dir(0) {}

	Pic(int _ID, int _Value, int _Dir) : ID(_ID), Value(_Value), Dir(_Dir) {}
	Pic* Clone() { return new Pic(this->ID, this->Value, this->Dir); }

	inline bool operator==(const Pic &obj) const
	{
		return this->ID == obj.ID && this->Value == obj.Value && this->Dir == obj.Dir;
	}
	struct ptrEqual {
		inline bool operator()(const Pic *obj1, const Pic *obj2) const { return *obj1 == *obj2; }
	};
	struct hashFunction {
		inline size_t operator()(const Pic &obj) const
		{
			size_t res = 2166136261;
			res = (res * 16777619) ^ hash<int>()(obj.ID);
			res = (res * 16777619) ^ hash<int>()(obj.Value);
			res = (res * 16777619) ^ hash<int>()(obj.Dir);
			return res;
		}
		inline size_t operator()(const Pic *obj) const { return hashFunction::operator()(*obj); }
	};
	// filter example: [&](const Pic* p) { return p->ID % 2 == 0; }
	static vector<Pic*> findAll(vector<Pic*> &vec, function<bool(Pic*)> filter)
	{
		vector<Pic*> res;
		copy_if(vec.begin(), vec.end(), back_inserter(res), filter);
		return res;
	}
	static inline bool CompareByID(const Pic &obj1, const Pic &obj2) { return obj1.ID < obj2.ID; }
	static inline bool CompareByIDPtr(const Pic *obj1, const Pic *obj2) { return obj1->ID < obj2->ID; }
	static inline bool CompareByValue(const Pic &obj1, const Pic &obj2) { return obj1.Value < obj2.Value; }
	static inline bool CompareByValuePtr(const Pic *obj1, const Pic *obj2) { return obj1->Value < obj2->Value; }
	static inline bool CompareByDir(const Pic &obj1, const Pic &obj2) { return obj1.Dir < obj2.Dir; }
	static inline bool CompareByDirPtr(const Pic *obj1, const Pic *obj2) { return obj1->Dir < obj2->Dir; }
	static inline bool CompareByIDValue(const Pic &obj1, const Pic &obj2)
	{
		if (obj1.ID == obj2.ID) {
			return obj1.Value < obj2.Value;
		}
		else return obj1.ID < obj2.ID;
	}
	static inline bool CompareByIDValuePtr(const Pic *obj1, const Pic *obj2)
	{
		if (obj1->ID == obj2->ID) {
			return obj1->Value < obj2->Value;
		}
		else return obj1->ID < obj2->ID;
	}
	static inline void SortByID(vector<Pic> &vec) { std::sort(vec.begin(), vec.end(), Pic::CompareByID); }
	static inline void SortByID(vector<Pic*> &vec) { std::sort(vec.begin(), vec.end(), Pic::CompareByIDPtr); }
	static inline void SortByValue(vector<Pic> &vec) { std::sort(vec.begin(), vec.end(), Pic::CompareByValue); }
	static inline void SortByValue(vector<Pic*> &vec) { std::sort(vec.begin(), vec.end(), Pic::CompareByValuePtr); }
	static inline void SortByDir(vector<Pic> &vec) { std::sort(vec.begin(), vec.end(), Pic::CompareByDir); }
	static inline void SortByDir(vector<Pic*> &vec) { std::sort(vec.begin(), vec.end(), Pic::CompareByDirPtr); }
	static inline void SortByIDValue(vector<Pic> &vec) { std::sort(vec.begin(), vec.end(), Pic::CompareByIDValue); }
	static inline void SortByIDValue(vector<Pic*> &vec) { std::sort(vec.begin(), vec.end(), Pic::CompareByIDValuePtr); }
	string toString() const {
		stringstream ss;
		ss << "Pic(ID: " << ID << ", Value: " << Value << ", Dir: " << Dir << ")" << endl << "    (";
		for (size_t i = 0; i < tagIDs.size(); i++)
		{
			ss << tagIDs[i];
			if (i < tagIDs.size() - 1) ss << ", ";
		}
		ss << ")";
		return ss.str();
	}
	friend ostream& operator << (ostream &os, const Pic &obj) { os << obj.toString(); return os; }
	friend ostream& operator << (ostream &os, const Pic *obj) { os << *obj; return os; }
	static string ToString(vector<Pic> &vec)
	{
		stringstream ss;
		ss << endl << "Pic List:" << endl;
		for (int i = 0; i < vec.size(); i++)
			ss << vec[i] << endl;
		return ss.str();
	}
	static string ToString(vector<Pic*> &vec)
	{
		stringstream ss;
		ss << endl << "Pic List:" << endl;
		for (int i = 0; i < vec.size(); i++)
			ss << *vec[i] << endl;
		return ss.str();
	}
};

struct Edge
{
	int P1;
	int P2;
	int Score;

	Edge() : P1(0), P2(0), Score(0) {}

	Edge(int _P1, int _P2, int _Score) : P1(_P1), P2(_P2), Score(_Score) {}
	Edge* Clone() { return new Edge(this->P1, this->P2, this->Score); }

	inline bool operator==(const Edge &obj) const
	{
		return this->P1 == obj.P1 && this->P2 == obj.P2 && this->Score == obj.Score;
	}
	struct ptrEqual {
		inline bool operator()(const Edge *obj1, const Edge *obj2) const { return *obj1 == *obj2; }
	};
	struct hashFunction {
		inline size_t operator()(const Edge &obj) const
		{
			size_t res = 2166136261;
			res = (res * 16777619) ^ hash<int>()(obj.P1);
			res = (res * 16777619) ^ hash<int>()(obj.P2);
			res = (res * 16777619) ^ hash<int>()(obj.Score);
			return res;
		}
		inline size_t operator()(const Edge *obj) const { return hashFunction::operator()(*obj); }
	};
	// filter example: [&](const Edge* p) { return p->ID % 2 == 0; }
	static vector<Edge*> findAll(vector<Edge*> &vec, function<bool(Edge*)> filter)
	{
		vector<Edge*> res;
		copy_if(vec.begin(), vec.end(), back_inserter(res), filter);
		return res;
	}
	static inline bool CompareByP1(const Edge &obj1, const Edge &obj2) { return obj1.P1 < obj2.P1; }
	static inline bool CompareByP1Ptr(const Edge *obj1, const Edge *obj2) { return obj1->P1 < obj2->P1; }
	static inline bool CompareByP2(const Edge &obj1, const Edge &obj2) { return obj1.P2 < obj2.P2; }
	static inline bool CompareByP2Ptr(const Edge *obj1, const Edge *obj2) { return obj1->P2 < obj2->P2; }
	static inline bool CompareByScore(const Edge &obj1, const Edge &obj2) { return obj1.Score < obj2.Score; }
	static inline bool CompareByScorePtr(const Edge *obj1, const Edge *obj2) { return obj1->Score < obj2->Score; }
	static inline bool CompareByP1P2(const Edge &obj1, const Edge &obj2)
	{
		if (obj1.P1 == obj2.P1) {
			return obj1.P2 < obj2.P2;
		}
		else return obj1.P1 < obj2.P1;
	}
	static inline bool CompareByP1P2Ptr(const Edge *obj1, const Edge *obj2)
	{
		if (obj1->P1 == obj2->P1) {
			return obj1->P2 < obj2->P2;
		}
		else return obj1->P1 < obj2->P1;
	}
	static inline void SortByP1(vector<Edge> &vec) { std::sort(vec.begin(), vec.end(), Edge::CompareByP1); }
	static inline void SortByP1(vector<Edge*> &vec) { std::sort(vec.begin(), vec.end(), Edge::CompareByP1Ptr); }
	static inline void SortByP2(vector<Edge> &vec) { std::sort(vec.begin(), vec.end(), Edge::CompareByP2); }
	static inline void SortByP2(vector<Edge*> &vec) { std::sort(vec.begin(), vec.end(), Edge::CompareByP2Ptr); }
	static inline void SortByScore(vector<Edge> &vec) { std::sort(vec.begin(), vec.end(), Edge::CompareByScore); }
	static inline void SortByScore(vector<Edge*> &vec) { std::sort(vec.begin(), vec.end(), Edge::CompareByScorePtr); }
	static inline void SortByP1P2(vector<Edge> &vec) { std::sort(vec.begin(), vec.end(), Edge::CompareByP1P2); }
	static inline void SortByP1P2(vector<Edge*> &vec) { std::sort(vec.begin(), vec.end(), Edge::CompareByP1P2Ptr); }
	string toString() const {
		stringstream ss;
		ss << "Edge(P1: " << P1 << ", P2: " << P2 << ", Score: " << Score << ")";
		return ss.str();
	}
	friend ostream& operator << (ostream &os, const Edge &obj) { os << obj.toString(); return os; }
	friend ostream& operator << (ostream &os, const Edge *obj) { os << *obj; return os; }
	static string ToString(vector<Edge> &vec)
	{
		stringstream ss;
		ss << endl << "Edge List:" << endl;
		for (int i = 0; i < vec.size(); i++)
			ss << vec[i] << endl;
		return ss.str();
	}
	static string ToString(vector<Edge*> &vec)
	{
		stringstream ss;
		ss << endl << "Edge List:" << endl;
		for (int i = 0; i < vec.size(); i++)
			ss << *vec[i] << endl;
		return ss.str();
	}
};

struct Neigh
{
	int P1;
	int Score;
	int Common;

	Neigh() : P1(0), Score(0), Common(0) {}

	Neigh(int _P1, int _Score, int _Common) : P1(_P1), Score(_Score), Common(_Common) {}
	Neigh* Clone() { return new Neigh(this->P1, this->Score, this->Common); }

	inline bool operator==(const Neigh &obj) const
	{
		return this->P1 == obj.P1 && this->Score == obj.Score && this->Common == obj.Common;
	}
	struct ptrEqual {
		inline bool operator()(const Neigh *obj1, const Neigh *obj2) const { return *obj1 == *obj2; }
	};
	struct hashFunction {
		inline size_t operator()(const Neigh &obj) const
		{
			size_t res = 2166136261;
			res = (res * 16777619) ^ hash<int>()(obj.P1);
			res = (res * 16777619) ^ hash<int>()(obj.Score);
			res = (res * 16777619) ^ hash<int>()(obj.Common);
			return res;
		}
		inline size_t operator()(const Neigh *obj) const { return hashFunction::operator()(*obj); }
	};
	// filter example: [&](const Neigh* p) { return p->ID % 2 == 0; }
	static vector<Neigh*> findAll(vector<Neigh*> &vec, function<bool(Neigh*)> filter)
	{
		vector<Neigh*> res;
		copy_if(vec.begin(), vec.end(), back_inserter(res), filter);
		return res;
	}
	static inline bool CompareByP1(const Neigh &obj1, const Neigh &obj2) { return obj1.P1 < obj2.P1; }
	static inline bool CompareByP1Ptr(const Neigh *obj1, const Neigh *obj2) { return obj1->P1 < obj2->P1; }
	static inline bool CompareByScore(const Neigh &obj1, const Neigh &obj2) { return obj1.Score < obj2.Score; }
	static inline bool CompareByScorePtr(const Neigh *obj1, const Neigh *obj2) { return obj1->Score < obj2->Score; }
	static inline bool CompareByCommon(const Neigh &obj1, const Neigh &obj2) { return obj1.Common < obj2.Common; }
	static inline bool CompareByCommonPtr(const Neigh *obj1, const Neigh *obj2) { return obj1->Common < obj2->Common; }
	static inline bool CompareByP1Score(const Neigh &obj1, const Neigh &obj2)
	{
		if (obj1.P1 == obj2.P1) {
			return obj1.Score < obj2.Score;
		}
		else return obj1.P1 < obj2.P1;
	}
	static inline bool CompareByP1ScorePtr(const Neigh *obj1, const Neigh *obj2)
	{
		if (obj1->P1 == obj2->P1) {
			return obj1->Score < obj2->Score;
		}
		else return obj1->P1 < obj2->P1;
	}
	static inline void SortByP1(vector<Neigh> &vec) { std::sort(vec.begin(), vec.end(), Neigh::CompareByP1); }
	static inline void SortByP1(vector<Neigh*> &vec) { std::sort(vec.begin(), vec.end(), Neigh::CompareByP1Ptr); }
	static inline void SortByScore(vector<Neigh> &vec) { std::sort(vec.begin(), vec.end(), Neigh::CompareByScore); }
	static inline void SortByScore(vector<Neigh*> &vec) { std::sort(vec.begin(), vec.end(), Neigh::CompareByScorePtr); }
	static inline void SortByCommon(vector<Neigh> &vec) { std::sort(vec.begin(), vec.end(), Neigh::CompareByCommon); }
	static inline void SortByCommon(vector<Neigh*> &vec) { std::sort(vec.begin(), vec.end(), Neigh::CompareByCommonPtr); }
	static inline void SortByP1Score(vector<Neigh> &vec) { std::sort(vec.begin(), vec.end(), Neigh::CompareByP1Score); }
	static inline void SortByP1Score(vector<Neigh*> &vec) { std::sort(vec.begin(), vec.end(), Neigh::CompareByP1ScorePtr); }
	string toString() const {
		stringstream ss;
		ss << "Neigh(P1: " << P1 << ", Score: " << Score << ", Common: " << Common << ")";
		return ss.str();
	}
	friend ostream& operator << (ostream &os, const Neigh &obj) { os << obj.toString(); return os; }
	friend ostream& operator << (ostream &os, const Neigh *obj) { os << *obj; return os; }
	static string ToString(vector<Neigh> &vec)
	{
		stringstream ss;
		ss << endl << "Neigh List:" << endl;
		for (int i = 0; i < vec.size(); i++)
			ss << vec[i] << endl;
		return ss.str();
	}
	static string ToString(vector<Neigh*> &vec)
	{
		stringstream ss;
		ss << endl << "Neigh List:" << endl;
		for (int i = 0; i < vec.size(); i++)
			ss << *vec[i] << endl;
		return ss.str();
	}
};

struct Slide
{
	vector<Pic*> pics;
	vector<int>* tagIDs;

	Slide() {}

	Slide(Pic* p1)
	{
		pics.push_back(p1);
		tagIDs = &(p1->tagIDs);
	}

	Slide(Pic* p1, Pic* p2)
	{
		pics.push_back(p1);
		pics.push_back(p2);
		
		// TODO merge  p1 and p2 tagids
	}
};